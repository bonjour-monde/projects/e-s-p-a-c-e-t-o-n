**E S P A C E — N U M E R I Q U E**<br>
: :  Bonjour Monde<br>
: :  Université de Nîmes<br>
: :  18 février 2021<br>


————————————<br>
**C H E R C H E R  D U  C O D E**<br>
Coder, c'est avant tout savoir chercher des bouts de codes sur internet pour les intégrer à son propre travail. Savoir faire des recherches de manière indépendante, c'est aussi s'assurer de résoudre 90% de ses problèmes sans avoir à se référer à quelqu'un de plus expérimenté. Quand vous êtes bloqués, essayez de définir clairement votre problème et tappez telle quelle votre recherche sur google. La plupart du temps, vous tomberez sur [stackoverflow](https://stackoverflow.com/), un forum d'entre-aide où la première réponse est souvent la bonne. Côté problème de CSS, le superbe [CSS-Trick](https://css-tricks.com/) est blindé de tutoriels plus précis les uns que les autres.<br><br>
Si par contre, vous cherchez de l'inspiration, [CodePen](https://codepen.io) est une sorte de laboratoire ouvert où n'importe qui peut tester et partager des bouts de code. Il suffit de tapper dans la barre de recherche ce que vous cherchez à faire ('text animation', 'color gradient', etc.) pour trouver des dizaine de bouts de code prêt à être copiés collés. [CodeMyUi](https://codemyui.com/) est quant à elle une bibliothèque de script choisis permettant divers intéractions d'ui. Sur les deux derniers, il n'est pas impossible de tomber sur des scripts nécessitants d'utiliser javascript, faites donc attention ! <br>
<br>

————————————<br>
**L I E N S**<br>
[Lovely Eigenharp Alpha Performance](https://www.youtube.com/watch?v=9MR5qBtPRrM)<br>
[電磁祭囃子 in NEO TOKYO 2020](https://www.youtube.com/watch?v=A0VYsiMtrNE)<br>
<br>
[Your World of Text](https://www.yourworldoftext.com/)<br><br>
<br>



