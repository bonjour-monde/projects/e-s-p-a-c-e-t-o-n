**M O D U L A T I O N S — T Y P O G R A P H I Q U E S**<br>
: :  Bonjour Monde<br>
: :  Université de Nîmes<br>
: :  18 février 2021<br>


————————————<br>
**S U J E T**<br>
Vous proposerez des variations du caractères modulaire [Banquise](https://gitlab.com/bonjour-monde/fonderie/banquise-typeface/), répondant à des fonctions/effets de lecture. Vous penserez ces manipulations selon vos envies de narration déambulatoire et de spacialisation de la page Web. Vous garderez en tête les propriétés variables du caractère typographique — notamment en terme d'animation.



————————————<br>
**R E N D U**<br>
Chacun·e nous rendra au minimum deux fichiers typographiques au format Variable (.ttf). Ces deux explorations répondront à des "fonctions typographiques" différentes. Elles devront inclure au moins une manipulation de la forme-même du module "pixel", ainsi qu'une manipulation de l'agencement des modules.<br>
Un des deux fichiers typographiques sera une variation allant du caractère de base (Banquise Regular) à une version modifiée. L'autre fichier sera une variation ayant comme extrèmes deux versions modifiées.<br>
([Voir aide visuelle en cas de doute.](https://gitlab.com/bonjour-monde/projects/e-s-p-a-c-e-t-o-n/-/tree/master/_modulations-typographiques/ressources/rendu.pdf))




————————————<br>
**O R G A N I S A T I O N**<br>
—> Introduction au sujet<br>
—> Exercices courts d'initiation à [FontLab](https://www.fontlab.com/font-editor/fontlab/)<br>
—> Moment de discussion en binômes autour des effets/fonctions de lecture<br>
—> Travail personnel sur les variations typographiques<br>
—> Petit bilan avant plusieurs semaines de travail en autonomie


————————————<br>
**L I E N S**<br>
• [Fichiers sources du caractère Banquise](https://gitlab.com/bonjour-monde/fonderie/banquise-typeface/-/tree/master/sources/)<br>
• [Rappel des contrôles de base de FontLab (logiciel de type design)](https://gitlab.com/bonjour-monde/projects/e-s-p-a-c-e-t-o-n/-/blob/master/_modulations-typographiques/ressources/fontlab-help.pdf)<br>
• [Guide complet pour FontLab (logiciel de type design)](https://help.fontlab.com/fontlab/7/manual/Editing-an-Existing-Glyph/)<br>
• [Démonstration fontes variables](http://benjamindumond.fr/nimes/presentation/variable.html)<br>
• [Font Gauntlet, tester les fontes variables dans le navigateur](https://dinamodarkroom.com/gauntlet/)<br>
• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>

————————————<br>
**R É F É R E N C E S**<br>
• [Caractère typographique Gridlite, Rosetta Type Foundry](https://www.rosettatype.com/Gridlite)<br>
• [Caractère typographique Handjet, Rosetta Type Foundry](https://www.rosettatype.com/Handjet)<br>
• [Conférence de Toshi Omagari sur les caractères de jeux d'arcade](https://www.youtube.com/watch?v=wCwhwoW_-4w)<br>
• [V-fonts, exemples de fontes variables](https://v-fonts.com/)<br>
• [Caractère typographique Tiny, Velvetyne](http://velvetyne.fr/fonts/tiny/)<br>



