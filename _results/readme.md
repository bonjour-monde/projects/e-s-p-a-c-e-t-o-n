## Results from ‘Espace-Ton’ workshop, march-april 2021

![](img/EspaceTon-banner.gif)
Note: the typefaces produced by the students, available in this Gitlab repo, are shared ‘as is’. No work has been done on the naming or any other technical setting. Might need cleaned exports to be properly used.

## ————————————————————

#### Mickael & Yoann

![](img/LeMondedAlice-web.png)
![](img/LeMondedAlice-type.png)

#### Julie & Solène

![](img/LabyrintheDesEmotions-web.png)
![](img/LabyrintheDesEmotions-type.png)

#### Eva et Garance

![](img/Somnium-web.png)
![](img/Somnium-type.png)

#### Corentin & Beyza

![](img/Corps-web.png)
![](img/Corps-type.png)

#### Camille et Clara

![](img/LePotagerNimois-web.png)
![](img/LePotagerNimois-type.png)

#### Clara et Valentine

![](img/Ghibli-web.png)
![](img/Ghibli-type.png)

#### Léo et Lucas

![](img/Enquete-web.png)
![](img/Enquete-type.png)

#### Lucie & Imane

![](img/Extraordinaire-web.png)

#### Noémie et Juliette

![](img/CocktailParty-web.png)
![](img/CocktailParty-type.png)

#### Oranne & Lea

![](img/NotreFriperie-web.png)
![](img/NotreFriperie-type.png)

#### Emilie & Mélody

![](img/LesBainsDeYubaba-web.png)
![](img/LesBainsDeYubaba-type.png)

#### Alejandro & Baptiste

![](img/Portails-web.png)
![](img/Portails-type.png)

#### Enola & Flavie

![](img/StationEnovie-web.png)
![](img/StationEnovie-type.png)
