**E S P A C E  —  T O N**<br>
: :  Bonjour Monde<br>
: :  Université de Nîmes<br>
: :  février-mars 2021<br>


————————————<br>
**S U J E T**

Comment nous représentons-nous nos espaces numériques personnels, dans lesquels nous naviguons chaque jour ? Hérités des méthaphores liées aux espaces d'entreprise américaine, nous passons notre temps à gérer des dossiers, travailler sur bureau, accumuler puis jeter des fichiers à la corbeille et copier des notes dans notre presse-papier. Mais il ne s'agit ici que d'une convention imposée, dans laquelle nous sommes si immergés que nous ne pensons même pas qu'il puisse en être autrement. Pourquoi ne pas penser par exemple nos ordinateurs comme des chateaux, avec des dossiers «Porte sud», «Douves» et «Tour penchée» ?  Pourquoi ne pas les organiser selon les cartes du tarot, avec un dossier «Maison Dieu» dans lequel ranger les projets à première vue désagréables et qui finalement recèlent de bonnes surprises ? 

De la même manière, nous prenons pour acquis l'idée qu'une famille typographique se décline selon des variation de graisse et de chasse, le tout accompagné de version italique. Ces variations permettent pour les deux premières à  indiquer un titre, une emphase, voir un ton de voix puissant et pour la dernière à nous faire comprendre qu'un bout de phrase est par exemple dans une langue étrangère. Emphaser et marquer une langue étrangère sont-elles réellement les seules possibilités que l'on pourrait imaginer pour des déclinaisons typographiques ? Ne peut-on pas offrir au texte de nouvelles capacités ? Nous pourrions imaginer par exemple une déclinaison typographique servant à marquer le mensonge, ou une variation de caractère indiquant une voix chantante ou encore un écho. L’acte de lecture est une performance dont il ne tient qu'à nous d'élargir les possibilités

L'exercice que nous vous proposons est un travail de fiction, de mise en page et de dessin typographique. L'idée d'une narration déambulatoire, qui prendrait corps à travers la mise en page d'un espace numérique. Une sorte d'Histoire dont vous êtes le héros, où chaque page serait un fichier .html, tous reliés par des liens hypertext. Ce travail servira de prétexte à questionner deux notion : d'une part les manières que nous avons de construire nos espaces numériques personnels, et d'autre part les potentiels performatifs liés aux formes typographiques.


————————————<br>
**R E N D U**

Chaque binôme travaillera à une déambulation dans le navigateur, avec une réflexion sur la structure en dossiers, l'idée de parcours et la mise en forme graphique des "espaces" traversés. Cette mise en forme s'appuyera sur le caractère typographique Banquise, dont chaque étudiant·e produira au minimum une variation, par la modification de son module "pixel" de base et/ou le réagencement des différents modules composant les lettres et autres signes. L'ensemble des caractères typographiques produits sera mis à disposition de tou·te·s, afin d'être utilisés dans les narrations déambulatoires. Ces dernières ne devront pas nécessairement s'apparenter à récit, mais proposer une expérience de lecture originale, utilisant les propriétés des liens hypertext, la possible complexité d'une arborescence informatique ainsi que la fluidité et les qualités expressives d'une fonte variable.


————————————<br>
**C A L E N D R I E R**

— 4 FÉVRIER  : :  Présentation du sujet

— 18 FÉVRIER
– matin  : :  Exercice de code
– après-midi  : :  Exercice de manipulation typographique

— 11 MARS  : :  Suivi de projet

— 18 MARS  : :  Suivi de projet

— 25 MARS   : :  Rendu à distance


————————————<br>
**L I E N S**

Confrontation de mots (générateur)  : :  http://benjamindumond.fr/nimes/<br>
Confrontation de mots (résultats)  : :  https://semestriel.framapad.org/p/atelier_Nimes-typo

————————————<br>
**R É F É R E N C E S**

Beowolf  : :  https://letterror.com/fonts/beowolf.html<br>
David Foster Wallace, C'est de l'eau  : :  https://www.youtube.com/watch?v=ms2BvRbjOYo<br>
Stratégies Italiques  : :  http://www.strategiesitaliques.fr<br>
J. Cocteau, R. Massin, Les mariés de la Tour Eiffel  : :  https://www.flickr.com/photos/etiennepouvreau/49298379738/in/photostream/<br>
GlyphWorld typeface  : :  https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/<br>
Caractères typographiques aux axes de variation originaux  : :  https://v-fonts.com/fonts/gridlite https://v-fonts.com/fonts/flicker https://v-fonts.com/fonts/elastik https://v-fonts.com/fonts/schijn-variable https://v-fonts.com/fonts/tiny https://v-fonts.com/fonts/cheee-variable https://v-fonts.com/fonts/wind-vf https://v-fonts.com/fonts/whoa https://v-fonts.com/fonts/labil-grotesk-variable https://v-fonts.com/fonts/decovar
